import { Routes } from '@angular/router';

export const routes: Routes = [

  { path: 'login', loadComponent: () => import('./features/login.component')},
  { path: 'home', loadComponent: () => import('./features/home.component')},
  { path: 'demo1', loadComponent: () => import('./features/demo1.component')},
  { path: 'demo2', loadComponent: () => import('./features/demo2.component')},
  { path: 'demo3', loadComponent: () => import('./features/demo3.component')},
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];
