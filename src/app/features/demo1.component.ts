// app.component
import { Component, computed, effect, signal } from '@angular/core';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [CommonModule],
  template: `
    {{visible()}}
    <div *ngIf="isVisible()">Button Clicked!</div>
    <button
      (click)="visible.set(!visible())"
    >BTN</button>


    <label for="ofoewhofehwoifhewo">Your Name</label>
    <input type="text" id="ofoewhofehwoifhewo">
    
    <button
      (click)="toggle()"
    >BTN</button>
  `,
})
export default class Demo1Component {
  visible = signal(false);

  toggle()  {
    this.visible.update(s => !s)
  }

  isVisible = computed(() => this.visible())

}
