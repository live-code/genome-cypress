// app.component
import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <form
      [formGroup]="form"
      (ngSubmit)="sendHandler()"
    >
      <div>
        <label>First Name</label>
        <input
          type="text"
          formControlName="firstName"
        />
      </div>

      <button [disabled]="form.invalid || sent()" type="submit">
        {{sent() ? 'SENT' : 'SUBMIT'}}
      </button>
    </form>
  `,
})
export default class Demo2Component {
  fb = inject(FormBuilder)
  sent = signal(false)

  form = this.fb.group({
    firstName:[ '', { validators: [ Validators.required, Validators.minLength(3) ]}],
  })


  sendHandler() {
    console.log('cia')
    this.sent.set(true)
  }


}
