import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      demo3 works!
    </p>
  `,
  styles: [
  ]
})
export default class Demo3Component {

}
