describe('Demo 2 Page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo2')

    cy.contains('First Name')
      .siblings('input')
      .as('input')

    cy.contains('button', 'submit', { matchCase: false }).as('btn')

  })


  it("button is disabled when form is initialized", () => {
    cy.get('@btn').should('be.visible');
    cy.get('@btn').should('be.disabled');
    cy.get('@btn').contains('SUBMIT')
  })

  it("button is disabled if inputs are invalid", () => {
    cy.get('@input').type('Fa')
    cy.get('@btn').should('be.disabled');
    cy.get('@btn').contains('SUBMIT')
  });

  it("button is enabled if form is valid", () => {
    cy.get('@btn').should('be.disabled');
    cy.get('@input').type('Fabio}')
    cy.get('@btn').should('be.enabled');
  });

  it("button is disabled after sending", () => {
    cy.get('@input').type('Fabio')
    cy.get('button').contains('SUBMIT').should('be.visible')
    cy.get('@btn').click()
    cy.get('button').contains('SENT').should('be.visible')
  });


  it.skip('should ', function () {
    cy
      .contains('First Name')
      .siblings('input')
      .clear()
      .type('pippo')

    cy
      .contains('Subscribe')
      .siblings('input')
      .click()


    cy
      .contains('Date')
      .siblings('input')
      .type('2024-12-22')



    cy
      .contains('Food')
      .siblings('select')
      .select('3')


  });
});
