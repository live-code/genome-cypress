class LoginPage {

  elements = {
    username: () =>  cy.get('input[placeholder="Username"]'),
    password: () =>  cy.get('input[placeholder="Password"]'),
    button: () =>  cy.contains('SIGN IN'),
  }

  setMobile() {
   // cy.viewport(200, 200)
  }

  visit() {
    cy.visit('/login')
  }

  populate(u: string, p: string) {
    this.elements.username().type(u)
    this.elements.password().type(p)
  }

  signIn() {
    this.elements.button().click()
  }

  buttonIsDisable() {
    this.elements.button().should('be.disabled')
  }
  buttonIsEnable() {
    this.elements.button().should('be.enabled')
  }
}

export default new LoginPage()
