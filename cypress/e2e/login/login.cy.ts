import loginPage from './pages/loginPage';

describe("Playground", () => {
  beforeEach(() => {
    loginPage.setMobile()
    loginPage.visit();
  })

  it('the SignIn button should be disabled by default', () => {
    loginPage.buttonIsDisable();
  })

  it('the SignIn button should be disabled if validation fails', () => {
    loginPage.populate('ma', '12')
    loginPage.buttonIsDisable();
    cy.screenshots();
  })

  it('the SignIn button should be enabled if validation is successful', () => {
    loginPage.populate('mario', '12345')
    loginPage.buttonIsEnable();
  })

  it('visit home after login', () => {
    loginPage.populate('mario', '12345')
    loginPage.signIn()

    cy.url().should('include', '/home')

  })

});
