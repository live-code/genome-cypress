describe('login page', () => {
  beforeEach(() => {
    cy.visit('http://127.0.0.1:8090/_/')
  })

  it("user can sign-in", () => {
    cy.get('input[type="email"]').type('info@fabiobiondi.com')
    cy.get('input[type="password"]').type('1234567890')
    cy.get('button').contains('login', { matchCase: false}).click()

    cy.url().should('include', '/collections')

  })
  it("user cannot sign-in when wrong credentials", () => {
    cy.get('input[type="email"]').type('info@fabiobiondi.com')
    cy.get('input[type="password"]').type('123')
    cy.get('button').contains('login', { matchCase: false}).click()
    cy.contains('Invalid login credentials').should('be.visible')
  })


  it('user cannot sign-in if credentials are wrong', () => {
    cy.intercept(
      'http://127.0.0.1:8090/api/admins/auth-with-password',
      { method: 'POST'},
      {
        statusCode: 404,
        body: {"code":404,"message":"Failed to authenticate FABIO.","data":{}}
      }
    )
    cy.get('input[type="email"]').type('hello@test.com2');
    cy.get('input[type="password"]').type('x1234567890');
    cy.contains('Login').click()
    cy.contains('Invalid login credentials').should('be.visible')

  })

});
