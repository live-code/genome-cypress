
describe('Home page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200')

    cy.contains('First Name')
      .siblings('input')
      .as('input')

    cy.get('button')
      .as('btn')
  })

  it("button is disabled when form is initialized", () => {

    cy.get('@btn').should('be.visible')
    cy.get('@btn').should('be.disabled')
    cy.get('@btn').should('contain', 'SUBMIT')
  })


  it("button is disabled when validation fails", () => {
    cy.get('@input').type('Fa')
    cy.get('@btn').should('be.visible')
    cy.get('@btn').should('be.disabled')
    cy.get('@btn').should('contain', 'SUBMIT')
  })

  it("button is enabled when validation successed", () => {
    cy.get('@input').type('Fabio')
    cy.get('@btn').should('be.visible')
    cy.get('@btn').should('be.enabled')
    cy.get('@btn').should('contain', 'SUBMIT')
  })

  it("button is disabled after sending", () => {
    cy.get('@input').type('Fabio')
    cy.get('@btn').click()
    cy.get('@btn').should('contain', 'SENT')

    cy.get('@input').type('Fabio Biondi')
    cy.get('@btn').should('be.disabled')


  })

})
