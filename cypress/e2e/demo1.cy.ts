describe('Demo 1 Page', () => {
  before(() => {
    cy.visit('http://localhost:4200/demo1')
    cy
      .get('button')
      .contains('BTN')
      .as('btn')
  })


  it.only('should ', function  () {

    cy
      .contains('Your Name')
      .invoke('prop', 'for')
      .then(value => {
        cy.get('#' + value).type('pippo')
      })


  });

  it('should ', function  () {

    // Check if the element with the specified content exists without causing the test to fail
    // cy.contains('button', 'BTN').should('exist');

    const searchText = 'Button Clicked'
    // Perform the conditional test
    const el =  cy.contains('Button Clicked!').should('not.exist')
    console.log(el)


  });


  it('should hide msg when component is initialized', function () {
    cy.contains('Button Clicked!').should('not.exist')
  });

  it('should show msg when button is clicked', function () {
    cy
      .get('@btn')
      .click()

    cy
      .get('@btn')
      .should('be.enabled')
      .and('not.have.attr', 'role')

    cy.contains('Button Clicked!').should('be.visible')

    cy.get('@btn').then(el => {
      expect(el).to.be.enabled;
      expect(el).not.have.attr('role', 'button');
    })

    cy.contains('Button Clicked!').then(el => {
      expect(el).to.be.visible;
    })
  });

  it('should hide msg when button is clicked twice', function () {
    cy
      .get('@btn')
      .click()

    cy
      .get('@btn')
      .click()

    cy.contains('Button Clicked!').should('not.exist')
  });
});
