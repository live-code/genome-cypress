/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
import ViewportPreset = Cypress.ViewportPreset;

Cypress.Commands.add('signin', (u: string, p: string) => {
  cy.get('input[type="email"]').type(u)
  cy.get('input[type="password"]').type(p)
  cy.get('button').contains('login', { matchCase: false}).click()
})
Cypress.Commands.add('login', (u: string, p: string, signin: boolean) => {
  cy.get('input[placeholder="Username"]').type(u)
  cy.get('input[placeholder="Password"]').type(p)

  if (signin) {
    cy.contains('SIGN IN').click()
  }
})

Cypress.Commands.add('screenshots', () => {

  function screenshots() {
    const presets: ViewportPreset[] = ['iphone-5', 'samsung-note9']
    presets.forEach(preset => {
      cy.viewport(preset)
      cy.screenshot(`img_login_${preset}`, {
        onAfterScreenshot($el, props) {
          console.log($el, props)
          // props has information about the screenshot,
          // including but not limited to the following:
          // {
          //   name: 'my-screenshot',
          //   path: '/Users/janelane/project/screenshots/spec.cy.js/my-screenshot.png',
          //   size: '15 kb',
          //   dimensions: {
          //     width: 1000,
          //     height: 660,
          //   },
          //   scaled: true,
          //   blackout: [],
          //   duration: 2300,
          // }
        },
      })

    })

  }
})
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }


declare namespace Cypress {
  interface Chainable {
    login(email: string, password: string, signIn?: boolean): Chainable<void>
    signin(email: string, password: string): Chainable<void>
    screenshots(): Chainable<void>
  }
}
